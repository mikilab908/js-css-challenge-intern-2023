import userData from '../data/data.json' assert { type: 'json' };

let previewContainer = document.querySelector('.preview');
let layoutPlaceholder = document.querySelector('.layout-placeholder');

let cardContainer = document.createElement('div');
cardContainer.classList.add('card-container');

previewContainer.appendChild(cardContainer);

// single card design
function createCard(allUserdata) {
  let card = document.createElement('div');
  card.classList.add('card');

  let user = document.createElement('div');
  user.classList.add('user');

  let userInfo = document.createElement('div');
  userInfo.classList.add('user-info');

  let userNameDate = document.createElement('div');
  userNameDate.classList.add('user-name-date');

  let userPhoto = document.createElement('span');
  userPhoto.classList.add('user-photo');
  let userPhotoImage = document.createElement('img');
  userPhotoImage.src = allUserdata.profile_image;
  userPhoto.appendChild(userPhotoImage);

  let userName = document.createElement('h4');
  userName.classList.add('user-name');
  userName.innerText = allUserdata.name;

  let userDate = document.createElement('h5');
  userDate.classList.add('user-date');
  userDate.innerText = allUserdata.date;

  let socialIcon = document.createElement('div');
  let icon = document.createElement('img');
  if (allUserdata.source_type === 'facebook') {
    icon.src = './icons/facebook.svg';
  } else {
    icon.src = './icons/instagram-logo.svg';
  }

  socialIcon.appendChild(icon);

  let cardImage = document.createElement('div');
  cardImage.classList.add('card-image');
  let image = document.createElement('img');
  image.src = allUserdata.image;
  cardImage.appendChild(image);

  let content = document.createElement('p');
  content.innerText = allUserdata.caption;

  let likes = document.createElement('div');
  likes.classList.add('likes-box');

  let heart = document.createElement('img');
  heart.classList.add('likes-heart');
  heart.src = './icons/heart.svg';

  let likesCount = document.createElement('span');
  likesCount.innerHTML = allUserdata.likes;

  // adding a click function to the heart SVG
  // not finished
  heart.addEventListener('click', (e) => {
    console.log(e.target.current);
  });

  likes.appendChild(heart);
  likes.appendChild(likesCount);

  user.append(userInfo);
  user.appendChild(socialIcon);
  userInfo.append(userPhoto);

  userInfo.appendChild(userNameDate);
  userNameDate.append(userName);
  userNameDate.append(userDate);

  card.append(user);
  card.append(cardImage);
  card.append(content);
  card.append(likes);

  return card;
}

// function for drawing all cards with load button
let currentIndex = 0;

function drawCards() {
  let maxResult = 4;

  for (let i = 0; i < maxResult; i++) {
    const card = createCard(userData[i + currentIndex]);

    cardContainer.appendChild(card);
  }
  currentIndex += maxResult;

  if (currentIndex === userData.length) {
    loadButtonDiv.innerHTML = null;
  }

  layoutPlaceholder.innerHTML = null;
}

// the load button
const loadButtonDiv = document.createElement('div');
const loadButton = document.createElement('button');
loadButton.innerText = 'Load More';
loadButtonDiv.classList.add('load-button-div');
loadButton.classList.add('load-button');

loadButton.onclick = () => {
  drawCards();
};

loadButtonDiv.appendChild(loadButton);
previewContainer.appendChild(loadButtonDiv);

drawCards();
